package com.manelcc.t16ej1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TextView txtEstado;
    private Button btnEstado, btnBuscar, btnAcDes;
    private ListView lvLista;
    private WifiManager wfManager;
    private List<ScanResult> listWifiList;
    private BroadcastReceiver reciberWifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtEstado = (TextView) findViewById(R.id.txtEstado);
        btnEstado = (Button) findViewById(R.id.btnEstado);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        btnAcDes = (Button) findViewById(R.id.btnAcDes);
        lvLista = (ListView) findViewById(R.id.lvLista);

        wfManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        btnEstado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (wfManager.getWifiState()) {

                    case WifiManager.WIFI_STATE_ENABLED:
                        txtEstado.setText("Estado: Activado");
                        break;
                    case WifiManager.WIFI_STATE_ENABLING:
                        txtEstado.setText("Estado: Activando");
                        break;
                    case WifiManager.WIFI_STATE_DISABLED:
                        txtEstado.setText("Estado: Desactivado");
                        break;
                    case WifiManager.WIFI_STATE_DISABLING:
                        txtEstado.setText("Estado: Desactivando");
                        break;


                }
            }
        });

        btnAcDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wfManager.isWifiEnabled()) {
                    wfManager.setWifiEnabled(false);
                } else {
                    wfManager.setWifiEnabled(true);
                }
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wfManager.startScan();
                txtEstado.setText("Buscando redes...");

            }
        });

    }

    public class WifiReciver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.compareTo(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION) == 0) {
                listWifiList = wfManager.getScanResults();
                txtEstado.setText("redes encontradas");
                ArrayList<String> datos = new ArrayList<>();
                for (ScanResult result : listWifiList) {
                    datos.add(result.SSID);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, datos);
                lvLista.setAdapter(adapter);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(reciberWifi);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(reciberWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }
}
